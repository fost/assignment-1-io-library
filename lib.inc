%define SYS_EXIT 60
%define STDOUT 1
%define STDIN 0
%define INPUT 0
%define OUTPUT 1

;Did not include registers into defines in order 
;To see explicit assignments at syscalls

section .text

;Params:
;	rdi: Return status
;Out:
;	!exit
exit:
    mov rax, SYS_EXIT
    syscall

;Params:
;	rdi: String link
;Out:
;	rax: String length
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

;Params:
;	rdi: String link
;Out:
;	!stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, OUTPUT
    mov rdi, STDOUT
    syscall
    ret

;Params:
;	-
;Out:
;	!stdout
print_newline:
    mov rdi, '\n'

;Params:
;	rdi: Code of char
;Out:
;	!stdout
print_char:
    mov rax, OUTPUT
    push rdi
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

;Params:
;	rdi: Unsigned 8-byte decimal number
;Out:
;	!stdout
print_int:
    cmp rdi, 0
    jns .print_uns
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.print_uns:

;Params:
;	rdi: Unsigned 8-byte decimal number
;Out:
;	!stdout
print_uint:
    mov r8, 10
    mov rax, rdi
    xor rcx, rcx
    inc rcx
    dec rsp
    mov byte [rsp], 0
.loop:
    xor rdx, rdx
    div r8
    add rdx, '0'
    dec rsp
    mov byte [rsp], dl
    inc rcx
    test rax, rax
    jnz .loop
    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    add rsp, rcx
    ret

;Params:
;	rdi: First string link
;	rsi: Second string link
;Out:
;	rax: Result
string_equals:
    xor rcx, rcx
.loop:
    mov al, [rdi+rcx]
    cmp al, [rsi+rcx]
    jne .not_equal
    cmp al, 0
    je .equal
    inc rcx
    jmp .loop
.not_equal:
    mov rax, 0
    ret
.equal:
    mov rax, 1
    ret

;Params:
;	!stdin
;Out:
;	rax: 	Char from stdin
;		0 if the end of the stream is reached
read_char:
    mov rax, INPUT
    mov rdi, STDIN
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

;Params:
;	rdi: Buffer string link
;	rsi: Buffer string size   
;Out:
;	rax:	Filled buffer link
;		0 if operation failed
;	rdx: Filled buffer size	
read_word:
    push r12
    push r13
    push r14
    xor r14, r14
    mov r12, rsi
    mov r13, rdi
.loop:
    mov rdi, r13
    call read_char
    cmp r14, r12
    je .failure_exit
    cmp rax, ` `
    je .space_char
    cmp rax, `\t`
    je .space_char
    cmp rax, `\n`
    je .space_char
    mov byte [r13+r14], al
    cmp rax, 0
    je .success_exit
    inc r14
    jmp .loop
.space_char:
    cmp r14, 0
    je .loop
    mov byte [r13+r14+1], 0 ; fall to success_exit
.success_exit:
    mov rdx, r14
    mov rax, r13
    jmp .exit
.failure_exit:
    mov rax, 0
.exit:
    pop r14
    pop r13
    pop r12
    ret

;Params:
;   	rdi: String link
;Out:
;	rax:	Decimal number length
;	rdx: 	Decimal number length
;		0 if operation failed
parse_uint:
    mov r9, `\n`
    xor rax, rax
    xor rcx, rcx
.loop:
    movzx r8, byte [rdi+rcx]
    cmp r8b, 0
    je .end
    sub r8b, '0'
    cmp r8b, 9
    ja .end ; if not digit - exit
    mul r9
    add rax, r8
    inc rcx
    jmp .loop
.end:
    mov rdx, rcx
    ret

;Params:
;	rdi: String link
;Out:
;	rdx: 	Number length
;		0 if operation failed
parse_int:
    movzx r8, byte [rdi]
    push r8
    cmp r8b, '-'
    jne .call_unsigned
    inc rdi
.call_unsigned:
    call parse_uint
    pop r8
    cmp r8b, '-'
    jne .end
    neg rax
    inc rdx
.end:
    ret 
;Params:
;	rdi: String link
;	rsi: Buffer string link
;	rdx: Buffer string size	
;Out:
;	rax:	String length
;		0 if buffer end reached
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    ja .failure_exit
    xor rcx, rcx
.loop:
    mov r8b, byte [rdi+rcx]
    mov byte [rsi+rcx], r8b
    inc rcx
    cmp rcx, rax
    jna .loop
    ret
.failure_exit:
    xor rax, rax
    ret